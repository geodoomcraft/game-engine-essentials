## Please view some images and gifs of the game here

https://imgur.com/a/31QqGy1

For this game project, I was mainly the person working on the networking but since it was a group project for University, I did help out with the other parts of the project.

---

## To download the game please use the following to try the game.

https://drive.google.com/file/d/1SuwA_hR-2WSsyHqSxRRZ6G3HblOc5wMv/view?usp=sharing

Instructions:
1. Download the files from link above
2. Extract files into a folder
3. Run 3D Pong.exe

Controls:
W,A,S,D to move
Space to Jump
Left Click to hit ball
Shift to run

---

## Unity version

Please use Unity version 2018.4.2f1 to ensure there are no issues when opening the project