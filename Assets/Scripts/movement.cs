﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movement : MonoBehaviour
{
    Rigidbody rb;
    public int downForce = 5;
    public int hitForce = 25;

    Animator _light;
    GameObject _tRend;

    int defaultForce;

    public AudioClip bounceClip;
    public AudioClip goalClip;
    public AudioSource bounceSource;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        _light = transform.GetComponent<Animator>();
        _tRend = transform.GetChild(1).gameObject;
        defaultForce = hitForce;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (rb.velocity.magnitude > hitForce) {
            rb.drag = 2;
        } else {
            rb.drag = 0;
        }

        if (transform.position.y > 8) {
            rb.AddForce(-transform.up * downForce);
        }
    }

    public void hitBall (Vector3 direction) {
        rb.velocity = Vector3.zero;
        rb.AddForce(direction * hitForce++, ForceMode.Impulse);
    }

    public void setColor(Color _color, bool isRed) {
        // _light.color = _color;
        _tRend.SetActive(true);
        if (isRed) {
            _light.ResetTrigger("setBlue");
            _light.ResetTrigger("reset");
            _light.SetTrigger("setRed");
        } else {
            _light.ResetTrigger("setRed");
            _light.ResetTrigger("reset");
            _light.SetTrigger("setBlue");
        }
        //_tRend.transform.GetComponent<TrailRenderer>().material.color = _color;
    }

    public void resetter() {
        hitForce = defaultForce;
        _light.ResetTrigger("setBlue");
        _light.ResetTrigger("setRed");
        _light.ResetTrigger("reset");
        _light.SetTrigger("reset");
        _tRend.SetActive(false);
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Wall")
        {
            bounceSource.clip = bounceClip;
            bounceSource.Play();
        }
        if (collision.gameObject.tag == "Goal")
        {
            bounceSource.clip = goalClip;
            bounceSource.Play();
        }
    }
}
