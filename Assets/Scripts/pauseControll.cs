﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pauseControll : MonoBehaviour
{

    public AudioSource pauseSource;
    public AudioClip elevatorClip;
    public PauseMenu pausemenuObj;

    // Start is called before the first frame update
    void Start()
    {
        pauseSource.clip = elevatorClip;
        pauseSource.time = 1.75f;
        pauseSource.Play();
        pauseSource.Pause();
    }
    
    public void StartPlay() {
        pauseSource.UnPause();
    }
    public void PausePlay() {
        pauseSource.Pause();
    }
}
