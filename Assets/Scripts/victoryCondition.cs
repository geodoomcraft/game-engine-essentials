﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class victoryCondition : MonoBehaviour
{

    public GameObject ball;

    public void setWinner(string winner) {
        this.GetComponent<Text>().text = winner + " Wins!";
        ball.SetActive(false);
        StartCoroutine(backToMenu());
    }

    IEnumerator backToMenu() {
        yield return new WaitForSeconds(3F);
        Cursor.lockState = CursorLockMode.None;
        SceneManager.LoadScene(0);
    }
}
