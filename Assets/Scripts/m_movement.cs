﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class m_movement : NetworkBehaviour
{
    Rigidbody rb;
    public int downForce = 5;
    public int hitForce = 25;

    Animator _light;
    GameObject _tRend;

    int defaultForce;

    public AudioClip bounceClip;
    public AudioClip goalClip;
    public AudioSource bounceSource;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        _light = transform.GetComponent<Animator>();
        _tRend = transform.GetChild(1).gameObject;
        defaultForce = hitForce;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (rb.velocity.magnitude > hitForce) {
            rb.drag = 2;
        } else {
            rb.drag = 0;
        }

        if (transform.position.y > 8) {
            rb.AddForce(-transform.up * downForce);
        }
    }

    [ClientRpc]
    public void RpcHitBall (Vector3 direction) {
        rb.velocity = Vector3.zero;
        rb.AddForce(direction * hitForce++, ForceMode.Impulse);
    }

    [ClientRpc]
    public void RpcSetColor(Color _color, bool isRed) {
        Debug.Log(isRed);
        _tRend.SetActive(true);
        if (isRed) {
            _light.ResetTrigger("setBlue");
            _light.ResetTrigger("reset");
            _light.SetTrigger("setRed");
            Debug.Log("Ball is Red");
        } else {
            _light.ResetTrigger("setRed");
            _light.ResetTrigger("reset");
            _light.SetTrigger("setBlue");
            Debug.Log("Ball is Blue");
        }
        //_tRend.transform.GetComponent<TrailRenderer>().material.color = _color;
    }

    [ClientRpc]
    public void RpcResetter() {
        hitForce = defaultForce;
        _light.ResetTrigger("setBlue");
        _light.ResetTrigger("setRed");
        _light.ResetTrigger("reset");
        _light.SetTrigger("reset");
        _tRend.SetActive(false);
        Debug.Log("Ball is White");
    }

    public void OnCollisionEnter(Collision collision)
    {
        //Tell server and clients to play sound
        if (collision.gameObject.tag == "Wall")
        {
            CmdPlaySounds("Wall");
        }
        if (collision.gameObject.tag == "Goal")
        {
            CmdPlaySounds("Goal");
        }
    }
    
    [Command]
    void CmdPlaySounds(string s)
    {
        if (s == "Wall")
        {
            RpcPlaySounds("Wall");
        }
        if (s == "Goal")
        {
            RpcPlaySounds("Goal");
        }
    }

    [ClientRpc]
    void RpcPlaySounds(string s)
    {
        if (s == "Wall")
        {
            bounceSource.clip = bounceClip;
            bounceSource.Play();
        }
        if (s == "Goal")
        {
            bounceSource.clip = goalClip;
            bounceSource.Play();
        }
    }
}
