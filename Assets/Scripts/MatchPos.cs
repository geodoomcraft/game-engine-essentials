﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchPos : MonoBehaviour
{
    public GameObject target;
    public int rotationSpeed = 1;
    public int lockedX = 15;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(target.transform.position.x, 0, lockedX);
        transform.Rotate(Vector3.up, rotationSpeed);
    }
}
