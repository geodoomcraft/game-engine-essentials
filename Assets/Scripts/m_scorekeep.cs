﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class m_scorekeep : NetworkBehaviour
{
    public GameObject scorecard;
    //SyncVar only update client's info when the server's value is changed
    [SyncVar(hook ="updateClientScore")]
    public int score = 0;
    public Transform prefab;


    void OnCollisionEnter(Collision col) {
        if (col.gameObject.layer == 9) {
            int newScore = score + 1;
            
            //Position of ball is synced with network transform
            col.gameObject.transform.GetComponent<Rigidbody>().velocity = Vector3.zero;
            col.gameObject.transform.position = new Vector3(0, 2, 0);
            //Reset function isn't synced, so must use commands and rpc
            CmdReset(col.gameObject);
            CmdUpdateScoreBoard(newScore);
            scorecard.GetComponent<Text>().text = score.ToString();
        }
    }

    [Command]
    void CmdUpdateScoreBoard(int newScore)
    {
        score = newScore;
    }

    //Call resetter on ball obj for server
    [Command]
    void CmdReset(GameObject obj)
    {
        obj.GetComponent<m_movement>().RpcResetter();
        // RpcReset(obj);
    }

    //function will be called when the score is updated on client side
    void updateClientScore(int newScore)
    {
        //When hook is called, the score does not automatically update
        score = newScore;
        scorecard.GetComponent<Text>().text = score.ToString();
    }

    // [ClientRpc]
    // //Call resetter on ball obj for all clients
    // void RpcReset(GameObject obj)
    // {
    //     obj.GetComponent<m_movement>().RpcResetter();
    // }
}
