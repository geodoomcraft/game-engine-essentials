﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hitBall : MonoBehaviour
{
    Color mainColor;
    public GameObject ball;
    public float hitRadius = 3;

    Rigidbody rb;
    Transform cam;
    Transform hitPoint;
    ParticleSystem ps;

    public AudioClip hitClip;
    public AudioSource playerSource;
    void Start()
    {
        ball = GameObject.FindGameObjectsWithTag("Ball")[0];
        cam = this.transform.GetChild(0);
        rb = ball.GetComponent<Rigidbody>();
        hitPoint = cam.GetChild(0);
        ps = ball.transform.GetChild(2).GetComponent<ParticleSystem>();

        mainColor = GetComponent<Renderer>().material.color;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0)) {
            if (Vector3.Distance(hitPoint.position, ball.transform.position) < hitRadius) {
                
                ball.GetComponent<movement>().hitBall(cam.transform.forward);
                ball.GetComponent<movement>().setColor(mainColor, false);

                ParticleSystem.MainModule main = ps.main;
                main.startColor = mainColor;
                ps.Play();

                playerSource.clip = hitClip;
                playerSource.Play();
            }
        }
    }
}
