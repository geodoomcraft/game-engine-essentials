﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scorekeep : MonoBehaviour
{
    public GameObject scorecard;
    public int score = 0;
    public int winCondition = 10;
    public string name;
    public Transform prefab;
    public Transform victory;
    public AudioClip goalClip;
    public AudioSource goalSorce;

    void OnCollisionEnter(Collision col) {
        if (col.gameObject.layer == 9) {
            score++;
            scorecard.GetComponent<Text>().text = score.ToString();
            col.gameObject.transform.GetComponent<Rigidbody>().velocity = Vector3.zero;
            col.gameObject.transform.position = new Vector3(0, 2, 0);
            col.gameObject.transform.GetComponent<movement>().resetter();
        }
        if (col.gameObject.tag == "Ball")
        {
            goalSorce.clip = goalClip;
            goalSorce.Play();
        }
        if (score == winCondition) {
            victory.GetComponent<victoryCondition>().setWinner(name);
        }
    }
}
