﻿using UnityEngine;
using System.Collections;


public class movePlayer : MonoBehaviour
{
    CharacterController characterController;

    public float sprintMulti = 2.0f;
    public float speed = 10.0f;
    public float jumpStrength = 100.0f;
    public bool isGrounded = true;

    Rigidbody rb;

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 11 && !isGrounded){
            isGrounded = true;
        }
    }

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        
        float translation = Input.GetAxis("Vertical") * speed;
        float straffe = Input.GetAxis("Horizontal") * speed;
        
        if (Input.GetKey(KeyCode.LeftShift)) {
            translation *= sprintMulti;
            straffe *= sprintMulti;
            
        }

        translation *= Time.deltaTime;
        straffe *= Time.deltaTime;
 
        transform.Translate(Vector3.right * straffe);
        transform.Translate(Vector3.forward * translation);

        transform.position = new Vector3(Mathf.Clamp(transform.position.x, -14f, 14f), transform.position.y, Mathf.Clamp(transform.position.z, -29f, -1f));

        if (Input.GetKeyDown(KeyCode.Space) && isGrounded){
            rb.AddForce(0, jumpStrength, 0, ForceMode.Impulse);
            isGrounded = false;
        }

        if (Input.GetKeyDown("escape")) {
            Cursor.lockState = CursorLockMode.None;
        }

        if (Input.GetKeyDown(KeyCode.Equals) && Time.timeScale == 1) {
            Time.timeScale = 0;
        } else if (Input.GetKeyDown(KeyCode.Equals) && Time.timeScale == 0) {
            Time.timeScale = 1;
        }
    }
}