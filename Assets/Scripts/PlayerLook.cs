﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLook : MonoBehaviour
{
    public float speedH = 2.0f;
    public float speedV = 2.0f;

    private float yaw = 0.0f;
    private float pitch = 0.0f;
    
    Transform character;

    void Start() {
        Cursor.lockState = CursorLockMode.Locked;
        character = transform.parent;
    }

    // Update is called once per frame
    void Update()
    {
        yaw += speedH * Input.GetAxis("Mouse X");
        pitch -= speedV * Input.GetAxis("Mouse Y");
        pitch = Mathf.Clamp(pitch, -90, 90);

        character.eulerAngles = new Vector3(0, yaw, 0);
        transform.localEulerAngles = new Vector3(pitch, 0, 0);
    }
}
