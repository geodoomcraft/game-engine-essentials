﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bgmController : MonoBehaviour
{

    public AudioSource bgmSource;
    public AudioClip bgmClip;
    public PauseMenu pausemenuObj;
    // Start is called before the first frame update
    void Start()
    {
        bgmSource.clip = bgmClip;
        bgmSource.Play();
    }

    // Update is called once per frame
    void Update()
    {
        if (pausemenuObj.isPaused)
        {
            if (Time.timeScale == 0f)
            {
                bgmSource.Pause();
            }
            
        }
        else
        {
            bgmSource.UnPause();
        }
    }
}
